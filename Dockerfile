FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ENV BSVERSION=0.26.3

EXPOSE 80

RUN mkdir -p /app/code/ /run/sessions /app/data
WORKDIR /app/code

# install requirements
RUN apt-get update && apt-get install -y php7.2-fpm php7.2-curl php7.2-mbstring php7.2-ldap \
    php7.2-tidy php7.2-xml php7.2-zip php7.2-gd php7.2-mysql libapache2-mod-php7.2 && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# get the code and extract it
RUN wget https://github.com/BookStackApp/BookStack/archive/v${BSVERSION}.tar.gz -O -| tar -xz -C /app/code --strip-components=1

RUN composer install

RUN mv /app/code/bootstrap/cache /app/code/bootstrap/cache_orig && \
    ln -s /app/data/cache /app/code/bootstrap/cache && \
    rm -rf /app/code/public/uploads && \
    ln -s /app/data/uploads /app/code/public/uploads && \
    mv /app/code/storage /app/code/storage_orig && \
    ln -s /app/data/storage /app/code/storage && \
    ln -s /app/data/.env /app/code/.env && \
    mv /app/code/public/.htaccess /app/code/public/.htaccess_orig && \
    ln -s /app/data/.htaccess /app/code/public/.htaccess && \
    chown www-data:www-data -R /app/code/bootstrap/cache /app/code/public/uploads /app/code/storage

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/bookstack.conf /etc/apache2/sites-enabled/bookstack.conf
RUN echo "Listen 80" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite php7.2
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

ADD start.sh /app/

RUN chown -R www-data.www-data /app/code /run/ /tmp /app/data

CMD [ "/app/start.sh" ]
