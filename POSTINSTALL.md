Login with your Cloudron account and adjust the settings for `Default user role after registration`, else everybody will get admin status after their first login.
