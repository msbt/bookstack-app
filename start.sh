#!/bin/bash

set -eux

if [[ ! -f /app/data/.env ]]; then
    echo "=> Detected first run"

    # set up default directories with write access and copy the data from readonly
    mkdir -p /app/data/cache /app/data/uploads /app/data/storage
    cp /app/code/.env.example /app/data/.env
    cp -r /app/code/storage_orig/* /app/data/storage
    cp -r /app/code/bootstrap/cache_orig/* /app/data/cache
    cp -r /app/code/public/.htaccess_orig /app/data/.htaccess

    #db setup
    sed -i "s/DB_HOST=.*/DB_HOST=${CLOUDRON_MYSQL_HOST}/" /app/data/.env
    sed -i "s/DB_DATABASE=.*/DB_DATABASE=${CLOUDRON_MYSQL_DATABASE}/" /app/data/.env
    sed -i "s/DB_USERNAME=.*/DB_USERNAME=${CLOUDRON_MYSQL_USERNAME}/" /app/data/.env
    sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=${CLOUDRON_MYSQL_PASSWORD}/" /app/data/.env
    # mail setup
    sed -i "s/MAIL_HOST=.*/MAIL_HOST=${CLOUDRON_MAIL_SMTP_SERVER}/" /app/data/.env
    sed -i "s/MAIL_PORT=.*/MAIL_PORT=${CLOUDRON_MAIL_SMTP_PORT}/" /app/data/.env
    sed -i "s/MAIL_USERNAME=.*/MAIL_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}/" /app/data/.env
    sed -i "s/MAIL_PASSWORD=.*/MAIL_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}/" /app/data/.env
    echo -e "\nMAIL_FROM=${CLOUDRON_MAIL_FROM=}" >> /app/data/.env

    # app url
    echo -e "\nAPP_URL=https://${CLOUDRON_APP_DOMAIN}" >> /app/data/.env

    # debug & ldap
    echo -e "\n#APP_DEBUG=true" >> /app/data/.env
    echo -e "\nAUTH_METHOD=ldap" >> /app/data/.env
    echo -e "LDAP_SERVER=${CLOUDRON_LDAP_SERVER}:${CLOUDRON_LDAP_PORT}" >> /app/data/.env
    echo -e "LDAP_BASE_DN=${CLOUDRON_LDAP_USERS_BASE_DN}" >> /app/data/.env
    echo -e "LDAP_DN=${CLOUDRON_LDAP_BIND_DN}" >> /app/data/.env
    echo -e "LDAP_PASS=${CLOUDRON_LDAP_BIND_PASSWORD}" >> /app/data/.env
    echo -e "LDAP_USER_FILTER=(&(username=\${user}))" >> /app/data/.env
    echo -e "LDAP_EMAIL_ATTRIBUTE=mail" >> /app/data/.env
    echo -e "LDAP_VERSION=3" >> /app/data/.env
    echo -e "LDAP_DISPLAY_NAME_ATTRIBUTE=username" >> /app/data/.env
    echo -e "LDAP_USER_TO_GROUPS=true" >> /app/data/.env
    echo -e "LDAP_GROUP_ATTRIBUTE="memberOf"" >> /app/data/.env
    echo -e "LDAP_REMOVE_FROM_GROUPS=false" >> /app/data/.env

    # caching
    #echo -e "CACHE_DRIVER=redis" >> /app/data/.env
    #echo -e "SESSION_DRIVER=redis" >> /app/data/.env
    #echo -e "REDIS_SERVERS=${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}:0:${CLOUDRON_REDIS_PASSWORD}" >> /app/data/.env

    # generate key & migrate db
    cd /app/code/
    php artisan key:generate --no-interaction --force
    php artisan migrate --no-interaction --force

    # add settings for ldap login
    readonly mysql_cli="mysql -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h ${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-confirmation', 'false', NOW(), NOW());"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-enabled', 'false', NOW(), NOW());"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-restrict', '', NOW(), NOW());"
    $mysql_cli -e "INSERT INTO settings (setting_key, value, created_at, updated_at) VALUES ('registration-role', '1', NOW(), NOW());"
fi

if [[ ! -d /run/sessions/ ]]; then
    mkdir /run/sessions/
fi

chmod -R 755 /app/data/uploads /app/data/storage

echo "=> Ensuring runtime directories & env information"
chown -R www-data.www-data /app/data /run /tmp

sed -i "s/DB_HOST=.*/DB_HOST=${CLOUDRON_MYSQL_HOST}/" /app/data/.env
sed -i "s/DB_DATABASE=.*/DB_DATABASE=${CLOUDRON_MYSQL_DATABASE}/" /app/data/.env
sed -i "s/DB_USERNAME=.*/DB_USERNAME=${CLOUDRON_MYSQL_USERNAME}/" /app/data/.env
sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=${CLOUDRON_MYSQL_PASSWORD}/" /app/data/.env

sed -i "s/MAIL_HOST=.*/MAIL_HOST=${CLOUDRON_MAIL_SMTP_SERVER}/" /app/data/.env
sed -i "s/MAIL_PORT=.*/MAIL_PORT=${CLOUDRON_MAIL_SMTP_PORT}/" /app/data/.env
sed -i "s/MAIL_USERNAME=.*/MAIL_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}/" /app/data/.env
sed -i "s/MAIL_PASSWORD=.*/MAIL_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}/" /app/data/.env

sed -i "s,APP_URL=.*,APP_URL=https://${CLOUDRON_APP_DOMAIN}," /app/data/.env
sed -i "s/MAIL_FROM=.*/MAIL_FROM=${CLOUDRON_MAIL_FROM}/" /app/data/.env

sed -i "s/LDAP_SERVER=.*/LDAP_SERVER=${CLOUDRON_LDAP_SERVER}:${CLOUDRON_LDAP_PORT}/" /app/data/.env
sed -i "s/LDAP_BASE_DN=.*/LDAP_BASE_DN=${CLOUDRON_LDAP_USERS_BASE_DN}/" /app/data/.env
sed -i "s/LDAP_DN=.*/LDAP_DN=${CLOUDRON_LDAP_BIND_DN}/" /app/data/.env
sed -i "s/LDAP_PASS=.*/LDAP_PASS=${CLOUDRON_LDAP_BIND_PASSWORD}/" /app/data/.env

#sed -i "s,REDIS_SERVERS=.*,REDIS_SERVERS=${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}:0:${CLOUDRON_REDIS_PASSWORD}," /app/data/.env

# migrating & clearing cache after update
php artisan migrate --no-interaction --force
php artisan cache:clear
php artisan view:clear


echo "=> Run BookStack"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
